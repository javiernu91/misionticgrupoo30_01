package co.edu.uis.tienda01.servicios;

import co.edu.uis.tienda01.entidades.DetalleVenta;

public interface DetalleVentaServicio{
	DetalleVenta guardar(DetalleVenta detalleVenta);

}
