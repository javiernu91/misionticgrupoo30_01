package co.edu.uis.tienda01.servicios;

import java.util.List;
import java.util.Optional;

import co.edu.uis.tienda01.entidades.Usuario;
import co.edu.uis.tienda01.entidades.Venta;

public interface VentaServicio {
	
	List<Venta> encuentraTodos();
	Venta guardar(Venta venta);
	String generarNumeroFact();
	Optional<Venta> obtenerId(Integer id); 
	List<Venta> findByUsuario(Usuario usuario);
}
