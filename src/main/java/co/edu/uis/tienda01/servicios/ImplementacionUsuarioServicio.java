package co.edu.uis.tienda01.servicios;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import co.edu.uis.tienda01.entidades.Usuario;
import co.edu.uis.tienda01.repositorios.InterfaceUsuarioRepositorio;

@Service
public class ImplementacionUsuarioServicio implements UsuarioServicio{

	@Autowired
	private InterfaceUsuarioRepositorio usuarioRepository;
	
	@Override
	public Optional<Usuario> obtenerId(Integer id) {
		return usuarioRepository.findById(id);
	}

	@Override
	public Usuario guardar(Usuario usuario) {
		return usuarioRepository.save(usuario);
	}
	@Override
	public List<Usuario> encuentraTodos() {
		return usuarioRepository.findAll();
	}

	@Override
	public Optional<Usuario> obtenerEmail(String email) {
		return usuarioRepository.findByEmail(email);
	}

}
