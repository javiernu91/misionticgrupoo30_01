package co.edu.uis.tienda01.servicios;

import java.util.List;
import java.util.Optional;

import co.edu.uis.tienda01.entidades.Producto;

public interface ProductoServicio {
	
	//Metodos CRUD
	public Producto guardar(Producto producto);
	public Optional<Producto> obtenerId(Integer id);//El optional nos da la opción de validar si el producto existe o no
	public void actualizar(Producto producto);
	public void eliminar(Integer id);
	public List<Producto> encuentraTodos();

}
