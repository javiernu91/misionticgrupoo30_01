package co.edu.uis.tienda01.servicios;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import co.edu.uis.tienda01.entidades.Producto;
import co.edu.uis.tienda01.repositorios.InterfaceProductoRepositorio;

@Service
public class ImplementacionProductoServicio implements ProductoServicio {
	
	@Autowired //sirve para inyectar un objeto a esta clase
	private InterfaceProductoRepositorio interfaceProductoRepository;
	
	@Override
	public Producto guardar(Producto producto) {
		
		return interfaceProductoRepository.save(producto);
	}

	@Override
	public Optional<Producto> obtenerId(Integer id) {
		
		return interfaceProductoRepository.findById(id);
	}

	@Override
	public void actualizar(Producto producto) {
		interfaceProductoRepository.save(producto);
		
	}

	@Override
	public void eliminar(Integer id) {
		interfaceProductoRepository.deleteById(id);
		
	}

	@Override
	public List<Producto> encuentraTodos() {
		
		return interfaceProductoRepository.findAll();
	}

}
