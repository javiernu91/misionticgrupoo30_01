package co.edu.uis.tienda01.servicios;

import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Service;

import co.edu.uis.tienda01.entidades.Usuario;


public interface UsuarioServicio {
	
		Optional<Usuario> obtenerId(Integer id);
		Usuario guardar(Usuario usuario);
		//metodo que permite filtrar desde la impleemntación
		Optional<Usuario> obtenerEmail(String email);
		List<Usuario> encuentraTodos();
}
