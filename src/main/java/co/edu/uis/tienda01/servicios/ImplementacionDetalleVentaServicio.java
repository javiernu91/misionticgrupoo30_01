package co.edu.uis.tienda01.servicios;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import co.edu.uis.tienda01.entidades.DetalleVenta;
import co.edu.uis.tienda01.repositorios.InterfaceDetalleVentaRepositorio;

@Service
public class ImplementacionDetalleVentaServicio implements DetalleVentaServicio{

	@Autowired
	private InterfaceDetalleVentaRepositorio detalleVentaRepository;
	
	@Override
	public DetalleVenta guardar(DetalleVenta detalleVenta) {
		
		return detalleVentaRepository.save(detalleVenta);
	}

}
