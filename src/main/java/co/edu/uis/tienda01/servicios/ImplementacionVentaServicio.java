package co.edu.uis.tienda01.servicios;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import co.edu.uis.tienda01.entidades.Usuario;
import co.edu.uis.tienda01.entidades.Venta;
import co.edu.uis.tienda01.repositorios.InterfaceVentaRepositorio;

@Service
public class ImplementacionVentaServicio implements VentaServicio{

	@Autowired
	private InterfaceVentaRepositorio ventaRepository;
	
	@Override
	public Venta guardar(Venta venta) {
		return ventaRepository.save(venta);
	}

	@Override
	public List<Venta> encuentraTodos() {
		return ventaRepository.findAll();
	}

	//metodo que  genera el secuencial en string del número de la orden
	//porque llega como 000010 y lo pasamos a entero para poderlo trabajar
	@Override
	public String generarNumeroFact() {
		int numero=0;
		String numeroFactura=""; 
		List<Venta> ventas=encuentraTodos();
		List<Integer> numeros = new ArrayList<Integer>();
		ventas.stream().forEach(o -> numeros.add(Integer.parseInt(o.getNumero())));
		if (ventas.isEmpty()) {
			numero=1;
		}else {
			numero = numeros.stream().max(Integer::compare).get();
			numero++;
		}
		if (numero < 10) {
			numeroFactura="000000000" + String.valueOf(numero);
		}else if(numero <100) {
				numeroFactura="00000000" + String.valueOf(numero);
		}else if(numero <1000) {
			numeroFactura="0000000" + String.valueOf(numero);
		}else if(numero <10000) {
			numeroFactura="000000" + String.valueOf(numero);
		}
		return numeroFactura;
	}

	
	@Override
	public Optional<Venta> obtenerId(Integer id) {
		
		return ventaRepository.findById(id);
	}

	@Override
	public List<Venta> findByUsuario(Usuario usuario) {
		
		return ventaRepository.findByUsuario(usuario);
	}

}
