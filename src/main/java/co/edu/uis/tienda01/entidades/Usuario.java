package co.edu.uis.tienda01.entidades;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;



@Entity
@Table(name="usuarios")
public class Usuario {
		
		@Id
		@GeneratedValue(strategy = GenerationType.IDENTITY)
		private Integer id;
		private String nombre;
		private String email;
		private String direccion;
		private String telefono;
		private String tipo;
		private String password;
		@OneToMany(mappedBy="usuario") //mapeado de uno a muchos
		private List<Producto> productos; //hacia la tabla producto(un usuario o cliente tiene muchos productos)
		@OneToMany(mappedBy="usuario") //mapeado de uno a muchos 
		private List<Venta> ventas; //hacia la tabla ventas (un usuario o cliente tiene muchas ventas)
		
		//Constructor vacio
		public Usuario() {
			
		}

		public Usuario(Integer id, String nombre, String email, String direccion, String telefono, String tipo,
				String password) {
			super();
			this.id = id;
			this.nombre = nombre;
			this.email = email;
			this.direccion = direccion;
			this.telefono = telefono;
			this.tipo = tipo;
			this.password = password;
		}

		public Usuario(Integer id, String nombre, String email, String direccion, String telefono, String tipo,
				String password, List<Producto> productos, List<Venta> ventas) {
			super();
			this.id = id;
			this.nombre = nombre;
			this.email = email;
			this.direccion = direccion;
			this.telefono = telefono;
			this.tipo = tipo;
			this.password = password;
			this.productos = productos;
			this.ventas = ventas;
		}

		public Integer getId() {
			return id;
		}

		public void setId(Integer id) {
			this.id = id;
		}

		public String getNombre() {
			return nombre;
		}

		public void setNombre(String nombre) {
			this.nombre = nombre;
		}

		public String getEmail() {
			return email;
		}

		public void setEmail(String email) {
			this.email = email;
		}

		public String getDireccion() {
			return direccion;
		}

		public void setDireccion(String direccion) {
			this.direccion = direccion;
		}

		public String getTelefono() {
			return telefono;
		}

		public void setTelefono(String telefono) {
			this.telefono = telefono;
		}

		public String getTipo() {
			return tipo;
		}

		public void setTipo(String tipo) {
			this.tipo = tipo;
		}

		public String getPassword() {
			return password;
		}

		public void setPassword(String password) {
			this.password = password;
		}

		public List<Producto> getProductos() {
			return productos;
		}

		public void setProductos(List<Producto> productos) {
			this.productos = productos;
		}

		public List<Venta> getVentas() {
			return ventas;
		}

		public void setVentas(List<Venta> ventas) {
			this.ventas = ventas;
		}

		@Override
		public String toString() {
			return "Usuario [id=" + id + ", nombre=" + nombre + ", email=" + email + ", direccion=" + direccion
					+ ", telefono=" + telefono + ", tipo=" + tipo + ", password=" + password + ", productos="
					+ productos + ", ventas=" + ventas + "]";
		}
		
}
