package co.edu.uis.tienda01;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class Tienda01Application {

	public static void main(String[] args) {
		SpringApplication.run(Tienda01Application.class, args);
	}

}
