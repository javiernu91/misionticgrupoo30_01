package co.edu.uis.tienda01.repositorios;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import co.edu.uis.tienda01.entidades.Usuario;

@Repository
public interface InterfaceUsuarioRepositorio extends JpaRepository<Usuario, Integer>{
	//la funcionalidad del Optional en java 8 tipo de variable que permite almacenar 2 valores
	//primero:Objeto que necesitamos utilizar
	//segundo: valor null o empaty
	Optional<Usuario> findByEmail(String email);

}
