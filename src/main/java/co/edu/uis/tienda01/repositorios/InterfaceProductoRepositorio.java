package co.edu.uis.tienda01.repositorios;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import co.edu.uis.tienda01.entidades.Producto;

//Los repositorios en Spring son como los DAO en java
//El acceso a datos se realiza mediante DAO (Data Access Object), 
//con ello se obtiene un abstracción sobre el modelo de datos. 
//Solo se accede a los datos a través de métodos definidos en el DAO.
//Por intermedio de este repositorio vamos a realizar el CRUD para productos
@Repository
public interface InterfaceProductoRepositorio extends JpaRepository<Producto, Integer>{

}
