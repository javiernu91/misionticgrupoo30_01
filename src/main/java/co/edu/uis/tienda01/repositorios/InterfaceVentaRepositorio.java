package co.edu.uis.tienda01.repositorios;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import co.edu.uis.tienda01.entidades.Usuario;
import co.edu.uis.tienda01.entidades.Venta;

@Repository
public interface InterfaceVentaRepositorio extends JpaRepository<Venta, Integer>{
	
	List<Venta> findByUsuario(Usuario usuario);

}
