package co.edu.uis.tienda01.repositorios;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import co.edu.uis.tienda01.entidades.DetalleVenta;

@Repository
public interface InterfaceDetalleVentaRepositorio extends JpaRepository<DetalleVenta, Integer>{

}
