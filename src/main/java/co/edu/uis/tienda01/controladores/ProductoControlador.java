package co.edu.uis.tienda01.controladores;

import java.io.IOException;
import java.util.Optional;

import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import co.edu.uis.tienda01.entidades.Producto;
import co.edu.uis.tienda01.entidades.Usuario;
import co.edu.uis.tienda01.servicios.ProductoServicio;
import co.edu.uis.tienda01.servicios.SubirImagenServicio;
import co.edu.uis.tienda01.servicios.UsuarioServicio;

@Controller
@RequestMapping("/productos")
public class ProductoControlador {

	// Mensajes x consola reemplaza al System.out.println
	private final Logger LOGGER = LoggerFactory.getLogger(ProductoControlador.class);

	@Autowired
	private ProductoServicio productoService;

	@Autowired
	private SubirImagenServicio subirImagen;
	
	@Autowired
	private UsuarioServicio usuarioService;
	
	// variable de tipo Model para comunicar datos entre el propio controlador y la
	// vista
	@GetMapping("")
	public String mostrarProducto(Model model) {
		model.addAttribute("produ", productoService.encuentraTodos());
		return "productos/mostrarProducto";

	}

	@GetMapping("/crearP")
	public String crearP() {
		return "productos/crearProducto";
	}

	@PostMapping("/guardar")
	public String guardar(Producto producto, @RequestParam("img") MultipartFile file, HttpSession sesion) throws IOException {
		LOGGER.info("Este es el objeto del producto {}", producto);
		Usuario usu = usuarioService.obtenerId(Integer.parseInt(sesion.getAttribute("idUsuario").toString())).get();
		producto.setUsuario(usu);
		// guardar imagen
		if (producto.getId() == null) { // Cuando se crea un producto
			String nombreImagen = subirImagen.saveImage(file);
			producto.setImagen(nombreImagen);
		}
		productoService.guardar(producto);
		return "redirect:/productos";
	}

	@GetMapping("/editar/{id}")
	public String editar(@PathVariable Integer id, Model model) {
		Producto producto = new Producto();
		Optional<Producto> optionalProducto = productoService.obtenerId(id);
		producto = optionalProducto.get();
		LOGGER.info("Se encontro el siguiente producto {}", producto);
		model.addAttribute("product", producto);
		return "productos/editarProducto";
	}

	@PostMapping("/actualizar")
	public String actualizar(Producto producto, @RequestParam("img") MultipartFile file) throws IOException {
		Producto produc = new Producto();
		produc = productoService.obtenerId(producto.getId()).get();
		if (file.isEmpty()) { // editamos el producto pero no cambiamos la imagen
			producto.setImagen(produc.getImagen());
		} else {// cuando se edita tambien se cambia la imagen
			
			// eliminar cuando no sea la imagen por defecto
			if (!produc.getImagen().equals("defaul.jpg")) {
				subirImagen.eliminarImagen(produc.getImagen());
			}
			String nombreImagen = subirImagen.saveImage(file);
			producto.setImagen(nombreImagen);
		}
		producto.setUsuario(produc.getUsuario());
		productoService.actualizar(producto);
		return "redirect:/productos";
	}

	@GetMapping("/eliminar/{id}")
	public String eliminar(@PathVariable Integer id) {
		Producto p = new Producto();
		p = productoService.obtenerId(id).get();
		// eliminar cuando no sea la imagen por defecto
		if (!p.getImagen().equals("defaul.jpg")) {
			subirImagen.eliminarImagen(p.getImagen());
		}
		productoService.eliminar(id);
		return "redirect:/productos";
	}
}
