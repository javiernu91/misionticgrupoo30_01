package co.edu.uis.tienda01.controladores;

import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import co.edu.uis.tienda01.entidades.Producto;
import co.edu.uis.tienda01.entidades.Usuario;
import co.edu.uis.tienda01.entidades.Venta;
import co.edu.uis.tienda01.servicios.ProductoServicio;
import co.edu.uis.tienda01.servicios.UsuarioServicio;
import co.edu.uis.tienda01.servicios.VentaServicio;

@Controller
@RequestMapping("/administrador")
public class AdministradorControlador {
	
	@Autowired
	private ProductoServicio productoService;
	
	@Autowired
	private UsuarioServicio usuarioService;
	
	@Autowired
	private VentaServicio ventaService;
	
	private final Logger log = LoggerFactory.getLogger(AdministradorControlador.class); 
	
	@GetMapping("")
	public String home(Model model){
		List<Producto> productos = productoService.encuentraTodos();
		model.addAttribute("proto", productos);
		return "administrador/home";
	}
	@GetMapping("caracteristicas/{id}")
	public String caracteristicas(@PathVariable Integer id, Model model) {
		log.info("id producto enviado con parámetro {}",id );
		Producto pro= new Producto();
		Optional<Producto> optionalProducto = productoService.obtenerId(id);
		pro=optionalProducto.get();
		model.addAttribute("mostrarProducto", pro);
		return "administrador/caracteristicasProducto";
		
	}
	@GetMapping("/listarUsuarios")
	public String listarUusarios(Model model) {
		List<Usuario> usu = usuarioService.encuentraTodos();
		model.addAttribute("var_usuario", usu);
		return "administrador/usuarios-clientes";
	}
	@GetMapping("/ventas")
	public String ventas(Model model) {
		List<Venta> listarVentas = ventaService.encuentraTodos();
		model.addAttribute("var_venta",listarVentas);
		return "administrador/ventasGenerales";
	}
	@GetMapping("/detalleVenta/{id}")
	public String detalle(Model model, @PathVariable Integer id) {
		Venta venta = ventaService.obtenerId(id).get(); 
		model.addAttribute("var_detalle", venta.getDetalles());
		return "administrador/detalleVentas";
	}
}
