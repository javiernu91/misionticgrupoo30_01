package co.edu.uis.tienda01.controladores;

import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import co.edu.uis.tienda01.entidades.Usuario;
import co.edu.uis.tienda01.entidades.Venta;
import co.edu.uis.tienda01.servicios.UsuarioServicio;
import co.edu.uis.tienda01.servicios.VentaServicio;

@Controller
@RequestMapping("/usuario")
public class UsuarioControlador {
	
private final Logger log = LoggerFactory.getLogger(UsuarioControlador.class);
	
	@Autowired
	private UsuarioServicio usuarioService;//poder acceder a las operaciones CRUD
	
	@Autowired
	private VentaServicio ventaService;
	

	BCryptPasswordEncoder passEncriptado = new BCryptPasswordEncoder();
	
	
	
	//usuario/registro
	@GetMapping("/registro")
	public String crearUsuario() {
		return "usuarios/registroUsuario";
	}
	@PostMapping("/guardarUsuario")
	public String guardarUsuario(Usuario usuario) {
		log.info("usuario registro: {}", usuario);
		usuario.setTipo("Cliente");
		usuario.setPassword(passEncriptado.encode(usuario.getPassword()));
		usuarioService.guardar(usuario);
		return "redirect:/";
	}
	@GetMapping("/login")
	public String login() {
		return "usuarios/login";
	}
	@GetMapping("/acceder")
	public String acceder(Usuario usuario, HttpSession sesion) {
		log.info("Accesos: {}", usuario);
		//Optional<Usuario> user = usuarioService.obtenerEmail(usuario.getEmail());
		Optional<Usuario> user = usuarioService.obtenerId(Integer.parseInt(sesion.getAttribute("idUsuario").toString()));
		if (user.isPresent()) {
			sesion.setAttribute("idUsuario", user.get().getId());
			if (user.get().getTipo().equals("ADMIN")) {
				return "redirect:/administrador";
			}else {
				return "redirect:/";
			}
		}else {
			log.info("Usuario no existe");
		}
		return "redirect:/";
	}
	@GetMapping("/compras")
	public String obtenerCompras(Model model, HttpSession sesion) {
		model.addAttribute("var_sesion", sesion.getAttribute("idUsuario"));
		Usuario usuario = usuarioService.obtenerId(Integer.parseInt(sesion.getAttribute("idUsuario").toString())).get();
		List<Venta> ventas = ventaService.findByUsuario(usuario);
		model.addAttribute("var_ventas", ventas);
		return "usuarios/compras";
	}
	@GetMapping("/detalleCompra/{id}")
	public String detalleCompra(@PathVariable Integer id, HttpSession sesion, Model model) {
		
		//sesion
		model.addAttribute("var_sesion", sesion.getAttribute("idUsuario"));
		log.info("id de la orden: {}", id);
		Optional<Venta> ventas=ventaService.obtenerId(id);
		model.addAttribute("var_detalle", ventas.get().getDetalles());
		return "usuarios/detallecompra";
	}
	@GetMapping("/cerrar")
	public String CerrarSesion(HttpSession sesion) {
		sesion.removeAttribute("idUsuario");
		return "redirect:/";
	}
	
}
