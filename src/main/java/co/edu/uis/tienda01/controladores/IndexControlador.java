package co.edu.uis.tienda01.controladores;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpSession;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import co.edu.uis.tienda01.entidades.DetalleVenta;
import co.edu.uis.tienda01.entidades.Producto;
import co.edu.uis.tienda01.entidades.Usuario;
import co.edu.uis.tienda01.entidades.Venta;
import co.edu.uis.tienda01.servicios.DetalleVentaServicio;
import co.edu.uis.tienda01.servicios.ProductoServicio;
import co.edu.uis.tienda01.servicios.UsuarioServicio;
import co.edu.uis.tienda01.servicios.VentaServicio;

@Controller
@RequestMapping("/")
public class IndexControlador {
	
	private final Logger log = LoggerFactory.getLogger(IndexControlador.class);
	@Autowired
	private ProductoServicio productoService; 
	
	@Autowired
	private UsuarioServicio usuarioService;
	
	@Autowired
	private VentaServicio ventaService;
	
	@Autowired
	private DetalleVentaServicio detalleVentaService;
	
	
	
	//para almacenas los detalles de la orden
	List<DetalleVenta> var_detalles = new ArrayList<DetalleVenta>();
		//para almacenar datos de la Orden
		Venta venta = new Venta();
	
	@GetMapping("")
	public String home(Model model, HttpSession sesion) {
		log.info("sesión del usuario: {}", sesion.getAttribute("idUsuario"));
		model.addAttribute("proto", productoService.encuentraTodos());
		//sesion
		model.addAttribute("var_sesion", sesion.getAttribute("idUsuario"));
		return "usuarios/index";
	}
	@GetMapping("productoIndex/{id}")
	public String productoIndex(@PathVariable Integer id, Model model) {
		log.info("id producto enviado con parámetro {}",id );
		Producto pro= new Producto();
		Optional<Producto> optionalProducto = productoService.obtenerId(id);
		pro=optionalProducto.get();
		model.addAttribute("productos_index", pro);
		return "usuarios/productoIndex";
		
	}
	@PostMapping("/adicionarCarrito") 
	public String adicionarCarrito(@RequestParam Integer id, @RequestParam Integer var_cantidad, Model model) {
		DetalleVenta detalleVenta = new DetalleVenta();
		Producto producto = new Producto();
		double sumaTotal = 0;
		
		Optional<Producto> optionProducto = productoService.obtenerId(id);
		log.info("Producto añadido: {}", optionProducto.get());
		log.info("Cantidad: {}", var_cantidad);
		producto = optionProducto.get();
		
		detalleVenta.setCantidad(var_cantidad);
		detalleVenta.setPrecio(producto.getPrecio());
		detalleVenta.setNombre(producto.getNombre());
		detalleVenta.setTotal(producto.getPrecio()*var_cantidad);
		detalleVenta.setProductos(producto);//de aca depende la clave foranea
		
		//Validación para que el producto no se agregue mas de una vez
		Integer idProducto=producto.getId();
		boolean ingresado = var_detalles.stream().anyMatch(p -> p.getProducto().getId()==idProducto);
		if (!ingresado) {
			var_detalles.add(detalleVenta);
		}
		sumaTotal = var_detalles.stream().mapToDouble(dt->dt.getTotal()).sum();
		venta.setTotal(sumaTotal);
		model.addAttribute("carrito_compras", var_detalles);
		model.addAttribute("fact", venta);
		return "usuarios/carrito";
	}
	//Quitar producto del carrito de compras
	@GetMapping("eliminar/carrito/{id}")
	public String eliminarProductoCarrito(@PathVariable Integer id, Model model) {
			//Lista nuevos productos
			List<DetalleVenta> ventaNueva = new ArrayList<DetalleVenta>();
			for (DetalleVenta detalleVenta: var_detalles) {
				if (detalleVenta.getProducto().getId()!=id) {
					ventaNueva.add(detalleVenta);
							
				}
			}
			//Poner la nueva lista con los productos restantes
	        var_detalles=ventaNueva;
			double sumaTotal=0;
			sumaTotal = var_detalles.stream().mapToDouble(dt->dt.getTotal()).sum();
			venta.setTotal(sumaTotal);
			model.addAttribute("carrito_compras", var_detalles);
			model.addAttribute("fact", venta);
			return "usuarios/carrito";
		}
	@GetMapping("/obtenerCarrito")
	public String obtenerCarrito(Model model, HttpSession sesion){
		
		model.addAttribute("carrito_compras", var_detalles);
		model.addAttribute("fact", venta);
		//sesion
		model.addAttribute("var_sesion", sesion.getAttribute("idUsuario"));
		return "/usuarios/carrito";
	}
	
	@GetMapping("/venta")
	public String venta(Model model, HttpSession sesion) {
		
		Usuario usuario = usuarioService.obtenerId(Integer.parseInt(sesion.getAttribute("idUsuario").toString())).get();
		
		model.addAttribute("carrito_compras", var_detalles);
		model.addAttribute("fact", venta);
		model.addAttribute("vistaUsuario", usuario);
		
		
		return "/usuarios/resumenVenta";
	}
	//guardar la venta
	@GetMapping("/guardarVenta")
	public String guardarVenta(RedirectAttributes redirectAttrs, HttpSession sesion) {
		Date fechaCreacion = new Date();
		venta.setFechaCreacion(fechaCreacion);
		venta.setNumero(ventaService.generarNumeroFact());
		//usuario
		Usuario usuario = usuarioService.obtenerId(Integer.parseInt(sesion.getAttribute("idUsuario").toString())).get();
		venta.setUsuario(usuario);
		ventaService.guardar(venta);
		//guardar detalles
		for (DetalleVenta dt: var_detalles) {
			dt.setVenta(venta);
			detalleVentaService.guardar(dt);
			}
			//Limpiar lista y orden
			redirectAttrs
				.addFlashAttribute("mensaje", "Se facturo Correctamente")
				.addFlashAttribute("clase", "success");
			venta = new Venta();
			var_detalles.clear();
			return "redirect:/";
		}
	@PostMapping("/buscar")
	public String buscarProducto(@RequestParam String var_nombre, Model model){
		log.info("Nombre del producto: {}",var_nombre );
		//Obtiene todos los productos productoService.findAll
		//Pasamos por un stream() es una API en java 8 permite realizar operaciones sobre la colección
		//como por ejemplo buscar, filtar, reordenar, reducir, etc
		//filtramos con filter que es una funcion lambda
		//le pasamos el predicado que es lo queremos hacer 
		//a traves de una ficcion fecha anonima trae el nombre del prodcuto
		//y el metodo contains le pasamos la cadena de caracteres que contengan ese nombre me lo devuelva en una lista
		List<Producto> productos = productoService.encuentraTodos().stream().filter(p ->p.getNombre().contains(var_nombre)).toList();
		model.addAttribute("proto", productos); //la lista que recibimos en el home y este de nombre proto
		
		return "usuarios/index";
	}
}
