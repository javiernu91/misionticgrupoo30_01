-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
-- -----------------------------------------------------
-- Schema proyectotienda01
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema proyectotienda01
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `proyectotienda01` DEFAULT CHARACTER SET utf8mb3 ;
USE `proyectotienda01` ;

-- -----------------------------------------------------
-- Table `proyectotienda01`.`categoria`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `proyectotienda01`.`categoria` (
  `idCategoria` INT NOT NULL AUTO_INCREMENT,
  `nombreCategoria` VARCHAR(50) NOT NULL,
  PRIMARY KEY (`idCategoria`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb3;


-- -----------------------------------------------------
-- Table `proyectotienda01`.`proveedor`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `proyectotienda01`.`proveedor` (
  `nitProve` VARCHAR(15) NOT NULL,
  `nombreProve` VARCHAR(50) NOT NULL,
  `direccionProve` VARCHAR(50) NOT NULL,
  `telefonoProve` VARCHAR(30) NOT NULL,
  PRIMARY KEY (`nitProve`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb3;


-- -----------------------------------------------------
-- Table `proyectotienda01`.`articulo`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `proyectotienda01`.`articulo` (
  `idArticulo` INT NOT NULL AUTO_INCREMENT,
  `nitProve` VARCHAR(15) NOT NULL,
  `idCategoria` INT NOT NULL,
  `nombreArticulo` VARCHAR(50) NOT NULL,
  `inventarioArticulo` INT NOT NULL,
  `precioArticulo` DOUBLE NOT NULL,
  PRIMARY KEY (`idArticulo`),
  INDEX `FK_nitProve_articulo_prove_idx` (`nitProve` ASC) VISIBLE,
  INDEX `FK_idCategoria_articulo_cate_idx` (`idCategoria` ASC) VISIBLE,
  CONSTRAINT `FK_idCategoria_articulo_cate`
    FOREIGN KEY (`idCategoria`)
    REFERENCES `proyectotienda01`.`categoria` (`idCategoria`),
  CONSTRAINT `FK_nitProve_articulo_prove`
    FOREIGN KEY (`nitProve`)
    REFERENCES `proyectotienda01`.`proveedor` (`nitProve`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb3;


-- -----------------------------------------------------
-- Table `proyectotienda01`.`cliente`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `proyectotienda01`.`cliente` (
  `idCliente` VARCHAR(15) NOT NULL,
  `nombreCliente` VARCHAR(50) NOT NULL,
  `direccionCliente` VARCHAR(50) NOT NULL,
  `telefonoCliente` VARCHAR(30) NOT NULL,
  PRIMARY KEY (`idCliente`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb3;


-- -----------------------------------------------------
-- Table `proyectotienda01`.`venta`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `proyectotienda01`.`venta` (
  `idVenta` INT NOT NULL AUTO_INCREMENT,
  `idCliente` VARCHAR(15) NOT NULL,
  `fechaVenta` DATETIME NOT NULL,
  `descuento` DOUBLE NULL DEFAULT NULL,
  `totalVenta` DOUBLE NOT NULL,
  PRIMARY KEY (`idVenta`),
  INDEX `FK_idCliente_venta_cliente_idx` (`idCliente` ASC) VISIBLE,
  CONSTRAINT `FK_idCliente_venta_cliente`
    FOREIGN KEY (`idCliente`)
    REFERENCES `proyectotienda01`.`cliente` (`idCliente`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb3;


-- -----------------------------------------------------
-- Table `proyectotienda01`.`detalleventa`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `proyectotienda01`.`detalleventa` (
  `idArticulo` INT NOT NULL,
  `idVenta` INT NOT NULL,
  `cantidadVenta` INT NOT NULL,
  `totalVentaArticulo` DOUBLE NOT NULL,
  INDEX `fk_idVenta` (`idVenta` ASC) VISIBLE,
  INDEX `fk_idArticulo` (`idArticulo` ASC) VISIBLE,
  CONSTRAINT `FK_idArticulo_DV_articulo`
    FOREIGN KEY (`idArticulo`)
    REFERENCES `proyectotienda01`.`articulo` (`idArticulo`),
  CONSTRAINT `FK_idVenta_DV_venta`
    FOREIGN KEY (`idVenta`)
    REFERENCES `proyectotienda01`.`venta` (`idVenta`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb3;


-- -----------------------------------------------------
-- Table `proyectotienda01`.`usuarios`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `proyectotienda01`.`usuarios` (
  `idUsuario` INT NOT NULL AUTO_INCREMENT,
  `nomUsurio` VARCHAR(50) NULL DEFAULT NULL,
  `perfilUsuario` VARCHAR(30) NULL DEFAULT NULL,
  `telefonoUsuario` VARCHAR(30) NULL DEFAULT NULL,
  PRIMARY KEY (`idUsuario`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb3;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
